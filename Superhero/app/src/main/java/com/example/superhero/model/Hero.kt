package com.example.superhero.model;

import androidx.annotation.StringRes;

data class Hero(
        val nameRes: Int,
        val descriptionRes: Int,
        val imageRes: Int
)
