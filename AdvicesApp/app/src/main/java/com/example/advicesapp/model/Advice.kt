package com.example.advicesapp.model

data class Advice(
    val nameRes: Int,
    val descriptionRes: Int,
    val imageRes: Int
)
