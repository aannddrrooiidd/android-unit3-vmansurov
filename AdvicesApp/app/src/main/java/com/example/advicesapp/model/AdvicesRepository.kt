package com.example.advicesapp.model

import com.example.advicesapp.R

object AdvicesRepository {
    val advices = listOf<Advice>(
        Advice(
            nameRes = R.string.advice1,
            descriptionRes = R.string.description1,
            imageRes = R.drawable.image3
//            imageRes = R.drawable.advice1
        ),
        Advice(
            nameRes = R.string.advice2,
            descriptionRes = R.string.description2,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice2
        ),
        Advice(
            nameRes = R.string.advice3,
            descriptionRes = R.string.description3,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice3
        ),
        Advice(
            nameRes = R.string.advice4,
            descriptionRes = R.string.description4,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice4
        ),
        Advice(
            nameRes = R.string.advice5,
            descriptionRes = R.string.description5,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice5
        ),
        Advice(
            nameRes = R.string.advice6,
            descriptionRes = R.string.description6,
            imageRes = R.drawable.advice6
        ),
        Advice(
            nameRes = R.string.advice7,
            descriptionRes = R.string.description7,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice7
        ),
        Advice(
            nameRes = R.string.advice8,
            descriptionRes = R.string.description8,
            imageRes = R.drawable.advice8
        ),
        Advice(
            nameRes = R.string.advice9,
            descriptionRes = R.string.description9,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice9
        ),
        Advice(
            nameRes = R.string.advice10,
            descriptionRes = R.string.description10,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice10
        ),
        Advice(
            nameRes = R.string.advice11,
            descriptionRes = R.string.description11,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice11
        ),
        Advice(
            nameRes = R.string.advice12,
            descriptionRes = R.string.description12,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice12
        ),
        Advice(
            nameRes = R.string.advice13,
            descriptionRes = R.string.description13,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice13
        ),
        Advice(
            nameRes = R.string.advice14,
            descriptionRes = R.string.description14,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice14
        ),
        Advice(
            nameRes = R.string.advice15,
            descriptionRes = R.string.description15,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice15
        ),
        Advice(
            nameRes = R.string.advice16,
            descriptionRes = R.string.description16,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice16
        ),
        Advice(
            nameRes = R.string.advice17,
            descriptionRes = R.string.description17,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice17
        ),
        Advice(
            nameRes = R.string.advice18,
            descriptionRes = R.string.description18,
//            imageRes = R.drawable.advice18
            imageRes = R.drawable.advice6
        ),
        Advice(
            nameRes = R.string.advice19,
            descriptionRes = R.string.description19,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice19
        ),
        Advice(
            nameRes = R.string.advice20,
            descriptionRes = R.string.description20,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice20
        ),
        Advice(
            nameRes = R.string.advice21,
            descriptionRes = R.string.description21,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice21
        ),
        Advice(
            nameRes = R.string.advice22,
            descriptionRes = R.string.description22,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice22
        ),
        Advice(
            nameRes = R.string.advice23,
            descriptionRes = R.string.description23,
            imageRes = R.drawable.advice6
//            imageRes = R.drawable.advice23
        ),
        Advice(
            nameRes = R.string.advice24,
            descriptionRes = R.string.description24,
            imageRes = R.drawable.advice24
        ),
        Advice(
            nameRes = R.string.advice25,
            descriptionRes = R.string.description25,
            imageRes = R.drawable.advice25
        ),
        Advice(
            nameRes = R.string.advice26,
            descriptionRes = R.string.description26,
            imageRes = R.drawable.advice26
        ),
        Advice(
            nameRes = R.string.advice27,
            descriptionRes = R.string.description27,
            imageRes = R.drawable.advice27
        ),
        Advice(
            nameRes = R.string.advice28,
            descriptionRes = R.string.description28,
            imageRes = R.drawable.advice28
        ),
        Advice(
            nameRes = R.string.advice29,
            descriptionRes = R.string.description29,
            imageRes = R.drawable.advice29
        ),
        Advice(
            nameRes = R.string.advice30,
            descriptionRes = R.string.description30,
            imageRes = R.drawable.advice30
        ),
    )
}